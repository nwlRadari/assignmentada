package main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * author: Dmytro Fomin ID 15898066 author: Nikolai Kolbenev ID 15897074
 *
 */
public class PageRank {
	public static final double DAMPING_FACTOR = 0.15d;
	private static HashMap<SpiderLeg, Integer> nodeToOutdegreeMap;
	private static HashMap<SpiderLeg, Integer> nodeToIndexMap;
	private static HashMap<Integer, SpiderLeg> indexToNodeMap;

	/**
	 * This is the only PUBLIC method here. Call it to update page rank field
	 * for each visited web page as per parameter.
	 * 
	 * We use a map because we need a two-way mapping from url to web node and
	 * back. It's easy to access url from a web node but not vice-versa!
	 * 
	 * @param visitedLegs.
	 *            This contains all visited nodes.
	 */
	public static void updatePageRanks(Map<String, SpiderLeg> visitedLegs) {
		if (visitedLegs.size() == 0) {
			return;
		}

		boolean[][] adjacencyMatrix = constructAdjacencyMatrix(visitedLegs);
		double[][] pageRankMatrix = constructPageRankMatrix(adjacencyMatrix);
		double[][] transposePageRank = getTranspose(pageRankMatrix);
		double[][] statDistrib = computePageRankCentrality(transposePageRank);

		// Update Page Ranks
		for (int i = 0; i < statDistrib.length; i++) {
			indexToNodeMap.get(i).setPageRank(statDistrib[i][0]);
		}
	}

	/**
	 * Ok first step is to construct adjacency matrix, establish indexing from a
	 * node to it's position in the matrix and calculate outdegree of every
	 * node. The matrix is used to check whether there is an edge from one node
	 * to another. Self-links are disallowed. Parallel edges are disallowed.
	 * Hyperlinks to unvisited pages must be ignored. We also need two-way
	 * mapping between index and node, use that to determine the presence of an
	 * edge and update the adjacency matrix accordingly. You can already see how
	 * complex these operations need to be.
	 * 
	 * @param legs.
	 *            All visited nodes.
	 */
	private static boolean[][] constructAdjacencyMatrix(Map<String, SpiderLeg> legs) {
		boolean[][] adjacencyMatrix = new boolean[legs.size()][legs.size()];

		// 100/75 is the inverse of load factor 0.75
		nodeToOutdegreeMap = new HashMap<>(legs.size() * 100 / 75);
		nodeToIndexMap = new HashMap<>(legs.size() * 100 / 75);
		indexToNodeMap = new HashMap<>(legs.size() * 100 / 75);
		int indexing = 0;

		for (String currentURL : legs.keySet()) {
			SpiderLeg currentNode = legs.get(currentURL);
			nodeToOutdegreeMap.put(currentNode, 0); // 0 degree initially
			if (!nodeToIndexMap.containsKey(currentNode)) {
				nodeToIndexMap.put(currentNode, indexing);
				indexToNodeMap.put(indexing, currentNode);
				indexing++;
			}

			HashSet<String> visitedOutURLs = new HashSet<>();
			ArrayList<String> allOutURLs = currentNode.getHyperLinks();
			for (String outURL : allOutURLs) {
				SpiderLeg outNode = legs.get(outURL);
				if (outNode != null && !outNode.equals(currentNode)) {
					// We only consider nodes that have actually been visited
					// We disallow self-edges and parallel edges
					if (!visitedOutURLs.contains(outURL)) {
						nodeToOutdegreeMap.put(currentNode, nodeToOutdegreeMap.get(currentNode) + 1);
						visitedOutURLs.add(outURL);
					}

					if (!nodeToIndexMap.containsKey(outNode)) {
						// We need this indexing to assign an edge in the matrix
						// below
						nodeToIndexMap.put(outNode, indexing);
						indexToNodeMap.put(indexing, outNode);
						indexing++;
					}

					adjacencyMatrix[nodeToIndexMap.get(currentNode)][nodeToIndexMap.get(outNode)] = true;
				}

			}
		}

		return adjacencyMatrix;
	}

	/**
	 * Second step is to construct the page rank matrix. We followed the
	 * algorithm as described in lecture slides.
	 * 
	 * To compute the matrix we need to estimate P(i, j). For this, we only need
	 * to know three things: n, out/indegree(i) and whether(i, j) is an edge.
	 * All of these have been captured during step one when constructing
	 * adjacency matrix.
	 * 
	 * We must ensure that outdegree includes only those nodes that we actually
	 * visited. getHyperlinks method returns all outgoing edges regardless of
	 * whether they have been visited! That's why we keep a seperate
	 * nodeToOutdegreeMap that stores the relevant outdegree of each node!
	 * 
	 * @param adjacencyMatrix
	 *            - The structure of the web.
	 * @return a page rank matrix.
	 */
	private static double[][] constructPageRankMatrix(boolean[][] adjacencyMatrix) {
		int n = adjacencyMatrix.length;
		double[][] pageRankMatrix = new double[n][n];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				SpiderLeg iLeg = indexToNodeMap.get(i);
				int outDegree = nodeToOutdegreeMap.get(iLeg);
				if (outDegree == 0) {
					// here i is a sink...
					pageRankMatrix[i][j] = 1.0d / n;
				} else if (adjacencyMatrix[i][j] == true) {
					pageRankMatrix[i][j] = (1.0d - DAMPING_FACTOR) / n + DAMPING_FACTOR / outDegree;
				} else {
					pageRankMatrix[i][j] = (1.0d - DAMPING_FACTOR) / n;
				}
			}
		}

		return pageRankMatrix;
	}

	/**
	 * Just a helper method to get a transpose of a page rank matrix.
	 * 
	 * @param matrix
	 * @return matrix transpose.
	 */
	private static double[][] getTranspose(double[][] matrix) {
		int matrixRowNum = matrix.length;
		int matrixColNum = matrix[0].length;

		double[][] result = new double[matrixColNum][matrixRowNum];

		for (int row = 0; row < matrixRowNum; row++) {
			for (int col = 0; col < matrixColNum; col++) {
				result[col][row] = matrix[row][col];
			}
		}

		return result;
	}

	/**
	 * Final third step. We use a numerical estimation for the stationary
	 * distribution. The amount of precision needed will depend on the number of
	 * web nodes visited. Hence, it's impossible to tell when the PageRank vector
	 * starts changing notably. Instead, we rely on 20 iterations as being
	 * optimal. No normalization is needed because it was accounted for in the
	 * page rank matrix itself (all probabilities there already add up to 1)
	 * 
	 * @param transposePageRank.
	 *            transpose of page rank matrix
	 * @return the stationary distribution
	 */
	private static double[][] computePageRankCentrality(double[][] transposePageRank) {
		// (A+I)(eigenvector) = multiplicand * multiplier
		double[][] statDistrib = getInitialStationaryDistribution(transposePageRank.length);

		for (int i = 0; i < 20; i++) {
			statDistrib = multiplyMatrices(transposePageRank, statDistrib);
		}

		System.out.print("Stationary distribution: (");
		for (int i = 0; i < statDistrib.length; i++) {
			System.out.printf("%.4f", statDistrib[i][0]);
			if (i + 1 < statDistrib.length) {
				System.out.print(", ");
			}
		}
		System.out.println(")");

		return statDistrib;
	}

	private static double[][] getInitialStationaryDistribution(int n) {
		double[][] eigenVector = new double[n][1];
		for (int i = 0; i < n; i++) {
			eigenVector[i][0] = 1.0d / n;
		}
		return eigenVector;
	}

	/**
	 * Just a helper method to multiply two matrices.
	 * 
	 * @param matrix1
	 * @param matrix2
	 * @return the product of matrix1 * matrix2
	 */
	private static double[][] multiplyMatrices(double[][] matrix1, double[][] matrix2) {
		int matrix1RowNum = matrix1.length;
		int matrix1ColNum = matrix1[0].length;
		int matrix2RowNum = matrix2.length;
		int matrix2ColNum = matrix2[0].length;

		if (matrix1ColNum != matrix2RowNum) {
			throw new IllegalStateException("Multiplication error: Matrix dimensions don't match");
		}

		double[][] product = new double[matrix1RowNum][matrix2ColNum];

		for (int row = 0; row < matrix1RowNum; row++) {
			for (int col = 0; col < matrix2ColNum; col++) {
				product[row][col] = 0;

				// Calculate the result of multiplying row by column, store it
				// in cell of "result" matrix
				int row_col_count = 0;
				while (row_col_count < matrix1ColNum) {
					product[row][col] += matrix1[row][row_col_count] * matrix2[row_col_count][col];
					row_col_count++;
				}
			}
		}

		return product;
	}

	/**
	 * Lab 7 Question 3 (c) Here we are just testing a graph from one of our
	 * labs. However, this graph does not reflect the extreme cases as present
	 * in the graphs of the Internet (parallel/self-edges and unvisited nodes).
	 * 
	 * Stationary distribution: (0.1835, 0.2111, 0.1923, 0.2135, 0.1996)
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpiderLeg leg1 = new SpiderLeg("1", 0, null, "");
		SpiderLeg leg2 = new SpiderLeg("2", 0, null, "");
		SpiderLeg leg3 = new SpiderLeg("3", 0, null, "");
		SpiderLeg leg4 = new SpiderLeg("4", 0, null, "");
		SpiderLeg leg5 = new SpiderLeg("5", 0, null, "");

		leg1.linksTest(true).add("2");
		leg2.linksTest(true); // Create new ArrayList
		leg3.linksTest(true).add("1");
		leg3.linksTest(true).add("2");
		leg3.linksTest(true).add("4");
		leg3.linksTest(true).add("5");
		leg4.linksTest(true).add("3");
		leg4.linksTest(true).add("5");
		leg5.linksTest(true).add("4");

		HashMap<String, SpiderLeg> map = new HashMap<>();
		map.put("1", leg1);
		map.put("2", leg2);
		map.put("3", leg3);
		map.put("4", leg4);
		map.put("5", leg5);

		updatePageRanks(map);
	}
}
