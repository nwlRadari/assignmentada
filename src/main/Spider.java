package main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import other.CustomDataHandler;
import other.Reusable;
import other.ThreadController;
import other.ThreadController.IThreadsActivityListener;

/**
 * author: Dmytro Fomin ID 15898066 author: Nikolai Kolbenev ID 15897074
 *
 * Initially, when search is started one spider is dispatched for each seed url.
 * But then every spider may try to generate new spiders for the urls yet to be
 * processed. Each spider runs on a separate thread. This way we can
 * simultaneously establish multiple connections and send numerous html
 * requests. Failed requests will be ignored by spider and it will continue with
 * exploring other nodes. The value of MAX_SPIDERS_RUNNING can be changed to 60
 * and beyond but 20 was more than enough for testing.
 * 
 */
public class Spider implements Runnable {
	public static boolean searchKeywords;
	public static boolean searchDescription;
	private static final int MAX_SPIDERS_RUNNING = 20;
	public static int urls_visited_num = 0;

	private static File fileToWriteVisited = new File("data", "visited.txt");
	private static File fileToWriteMatched = new File("data", "matched.txt");

	public static ThreadController threadController = new ThreadController();
	static {
		threadController.addThreadListener(new IThreadsActivityListener() {

			@Override
			public void beforeOnAllThreadsKilled() {
				if (visited != null && !visited.isEmpty()) {
					for (String failedLink : failedVisited) {
						visited.remove(failedLink);
					}
				}
				failedVisited = new LinkedList<>();
			}

			@Override
			public void onThreadDeath() {
			}

			@Override
			public void onThreadBirth() {
			}

			@Override
			public void onAllThreadsKilled() {
			}
		});
	}

	// If some link fails to connect after being added to visited - we need to
	// keep it there anyway
	// so that other spiders do not try process that node again. But when search
	// is
	// complete we must
	// remove that link from visited because we cannot retrieve HTML from it.
	private static Map<String, SpiderLeg> visited = new HashMap<>();
	private static List<String> failedVisited = new LinkedList<>();
	private static CustomDataHandler<SpiderLeg> matchedNodes = new CustomDataHandler(new HashSet<SpiderLeg>());
	static {
		// Default comparator
		matchedNodes.setLegsOrderingComparator(Reusable.getComparatorByPageRank());
	}

	private Queue<SpiderLeg> unvisited;
	private int distanceMax; // Distance that this spider will crawl from
	private SpiderLeg legStart; // Contains urlStart
	private String metaKeyword; // Keyword this spider will look for
	private final Spider spiderParent;

	private Thread threadCrawl;

	private Spider(Spider spiderParent, SpiderLeg legStart, int distanceMax, String metaKeyword) {
		unvisited = new LinkedList<>();

		this.distanceMax = distanceMax;
		this.metaKeyword = metaKeyword;
		this.spiderParent = spiderParent;
		this.legStart = legStart;

		unvisited.add(legStart);
	}

	/**
	 * The crawl version where multiple seed urls are supplied. Each seed will
	 * receive a new spider and each spider will call a local crawl.
	 * 
	 * @param seedURLs
	 * @param distanceMax.
	 *            The maximum distance a spider will travel from seed origin.
	 * @param metaKeyword.
	 *            The keyword to search for.
	 */
	public static void crawl(Collection<String> seedURLs, int distanceMax, String metaKeyword) {
		visited.clear();
		matchedNodes.clear();

		Reusable.buildPath(fileToWriteVisited);
		Reusable.buildPath(fileToWriteMatched);

		fileWrite("", fileToWriteVisited, false, false);
		fileWrite("", fileToWriteMatched, false, false);

		urls_visited_num = 0;
		// Two different spiders should not visit the same Node.
		for (String startURL : seedURLs) {
			SpiderLeg legStart = new SpiderLeg(startURL, 0, startURL, metaKeyword);
			Spider spider = new Spider(null, legStart, distanceMax, metaKeyword);
			spider.crawl();
		}
	}

	/**
	 * This local crawl is responsible for creating a thread for a spider to run
	 * the algorithm. You will find the actual algorithm inside the run()
	 * method.
	 */
	private void crawl() {
		if (threadCrawl != null) {
			System.err.println("Thread already running.");
			return;
		}

		threadCrawl = new Thread(this);
		threadCrawl.setDaemon(false);
		threadController.increaseThreadsRunning();
		threadCrawl.start();
	}

	/**
	 * The algorithm is split into 5 parts: 1) Obtain an unvisited node. 2) Try
	 * to generate a new spider to process that node. 3) If no spiders are
	 * available, go on with processing - add node to visited 4) Connect to that
	 * node and process it 5) Plan for any other nodes that must be visited.
	 * 
	 * If there are n seed urls, there will be n spiders that will have unique
	 * locations for their web starting points. Each spider will remain within
	 * the web that it was born in. The spider will not go onto a different web,
	 * it will simply travel a certain distance from it's web origin (seed url)
	 * and will stop if it has nothing else to explore.
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		Spider spiderRef = (this.spiderParent != null) ? this.spiderParent : this;

		while (!unvisited.isEmpty() && !threadController.isKillThreads()) {
			SpiderLeg nodeCurrent = unvisited.remove();

			// Try dispatch this task to a new spider (thread) if available.
			// Else remain on same thread and let this spider process it.
			if (unvisited.size() > 1) {
				boolean isSpiderAvailable = false;
				synchronized (this.threadController) {
					isSpiderAvailable = (threadController.getThreadsRunning() < MAX_SPIDERS_RUNNING);
					if (isSpiderAvailable) {
						Spider spider = new Spider(spiderRef, nodeCurrent, -1, this.getMetaKeyword());
						spider.crawl();
						continue;
					}
				}
			}

			// Using a synchronized block to add to a collection shared
			// by all spiders.
			synchronized (threadController) {
				if (visited.containsKey(nodeCurrent.getURL())) {
					// Some other spider has already started to
					// process/processed the current node
					continue;
				} else {
					// If we put the node into visited we must also proceed to
					// connect to
					// that node.
					visited.put(nodeCurrent.getURL(), nodeCurrent);
				}
			}

			// The only place where server connection is established
			nodeCurrent.connect();
			if (!nodeCurrent.wasHTMLRetreived) {
				System.err.println("Failed to retrieve HTML during spider search.");
				failedVisited.add(nodeCurrent.getURL());
				continue;
			}

			fileWrite(nodeCurrent.getURL(), fileToWriteVisited, true, true);

			urls_visited_num++;
			threadController.notifyVisitedURLsChanged();

			// Process current Node
			String meta = (searchKeywords) ? nodeCurrent.getMetaKeywords().trim() : "";
			meta += (searchDescription) ? nodeCurrent.getMetaDescription().trim() : "";

			if (meta.toLowerCase().contains(this.metaKeyword.toLowerCase())) {
				fileWrite(nodeCurrent.getURL(), fileToWriteMatched, true, true);
				matchedNodes.add(nodeCurrent);
				System.out.println("Keyword Matching: " + nodeCurrent.getURL());
			} else {
				System.out.println("Keyword Not Matching: " + nodeCurrent.getURL());
			}

			// Plan to visit neighbours
			int nextDistance = nodeCurrent.getDistance() + 1;
			if (nextDistance <= spiderRef.distanceMax) {
				ArrayList<String> links = nodeCurrent.getHyperLinks();
				for (String link : links) {
					if (threadController.isKillThreads()) {
						break; // Just in case we want to interrupt search
					}

					SpiderLeg nodeNeighbour = new SpiderLeg(link, nextDistance, spiderRef.legStart.getURL(),
							this.getMetaKeyword());
					if (!unvisited.contains(nodeNeighbour)) {
						unvisited.add(nodeNeighbour);
					}
				}
			}
		}

		threadController.decreaseThreadsRunning();
		threadCrawl = null;
	}

	/**
	 * We keep all visited nodes and all matched nodes in two different files
	 * that get populated during the search.
	 * 
	 * @param write.
	 *            Text to write
	 * @param file.
	 *            File to write to.
	 * @param append.
	 *            Whether to append to a file or to start over.
	 * @param newLine.
	 *            Whether to include a new line after the text.
	 */
	private static synchronized void fileWrite(String write, File file, boolean append, boolean newLine) {
		try {
			FileWriter fw = new FileWriter(file.getAbsolutePath(), append);
			if (newLine) {
				write += "\n";
			}
			fw.write(write);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static CustomDataHandler<SpiderLeg> getMatchedNodes() {
		return matchedNodes;
	}

	public static Map<String, SpiderLeg> getVisitedNodes() {
		return visited;
	}

	public String getMetaKeyword() {
		return metaKeyword;
	}

	public static void main(String[] args) {
		ArrayList<String> seedURLs = new ArrayList();
		seedURLs.add("https://www.javatpoint.com/jsoup-example-print-images-of-an-url");
		seedURLs.add("https://jsoup.org/");
		// seedURLs.add("https://drive.google.com/");

		Spider.crawl(seedURLs, 2, "tutorial");
	}
}
