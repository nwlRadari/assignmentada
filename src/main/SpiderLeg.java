package main;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import other.Constants;

/**
 * author: Dmytro Fomin ID 15898066
 * author: Nikolai Kolbenev ID 15897074
 * 
 * Ok, this is called SpiderLeg but we would call it a web segment instead. Yes,
 * SpiderLeg does just that - it connects to a webpage, parses HTML doc and
 * that's it. It will simply keep the data from parsed html that will be
 * accessible from Spider himself - because it's his leg, right?
 */
public class SpiderLeg {

	private Document docHtml;
	private ArrayList<String> links;

	private String urlSeed; // Origin
	private String url;
	private int distance; // Distance from seed URL (spider origin)
	private double pageRank = Constants.START_PAGE_RANK;
	public boolean wasHTMLRetreived;
	private String keyword;

	public SpiderLeg(String url, int distanceFromSpider, String urlSeed, String keyword) {
		this.url = url;
		this.distance = distanceFromSpider;
		this.urlSeed = urlSeed;
		this.keyword = keyword;
	}

	public void connect() {
		parse();
	}

	private void parse() {
		try {
			docHtml = Jsoup.connect(url).timeout(Constants.JSOUP_CONNECT_TIMEOUT).get();
			wasHTMLRetreived = true;
		} catch (Exception e) {
			e.printStackTrace();
			wasHTMLRetreived = false;
		}
	}

	public String getTitle() {
		return docHtml.title();
	}

	/**
	 * Using Jsoup to get hyperlinks. The document will be parsed once only.
	 * 
	 * @return a list of links.
	 */
	public ArrayList<String> getHyperLinks() {
		if (links != null) {
			return links;
		}

		links = new ArrayList<String>();
		Elements element = docHtml.select("a[href]");

		for (Element elem : element) {
			String link = elem.attr("href");
			if (!link.contains(" ")) {
				if (link.contains("http:") || link.contains("https:"))
					links.add(link);
				if (link.startsWith("//")) {
					link = "http:" + link;
					links.add(link);
				} else if (link.startsWith("www")) {
					link = "http://" + link;
					links.add(link);
				}
			}
		}
		return links;
	}

	public ArrayList<String> linksTest(boolean forTest) {
		if (!forTest) {
			return null;
		}
		// Only for use from PageRank algorithm main method!
		if (this.links == null) {
			this.links = new ArrayList<>();
		}
		return links;
	}

	/**
	 * Get html metadata, including description and keywords.
	 * 
	 * @return complete html metadata.
	 */
	public String getMeta() {
		Element keywords = docHtml.select("meta[name=keywords]").first();
		Element description = docHtml.select("meta[name=description]").first();

		String meta = (keywords != null) ? keywords.attr("content") : "";
		meta += (description != null) ? description.attr("content") : "";

		return meta;
	}

	/**
	 * Get html meta keywords.
	 * 
	 * @return html meta keywords
	 */
	public String getMetaKeywords() {
		Element keywords = docHtml.select("meta[name=keywords]").first();
		String meta = (keywords != null) ? keywords.attr("content") : "";

		return meta;
	}

	/** 
	 * Same here. Get html meta description.
	 * 
	 * @return html meta description
	 */
	public String getMetaDescription() {
		Element description = docHtml.select("meta[name=description]").first();
		String meta = (description != null) ? description.attr("content") : "";

		return meta;
	}

	/**
	 * 
	 * @return image data (see code)
	 */
	public String getImages() {
		StringBuilder strImages = new StringBuilder();

		Elements images = docHtml.select("img[src]");
		for (Element image : images) {
			strImages.append("src : " + image.attr("src"));
			strImages.append("\nheight : " + image.attr("height"));
			strImages.append("\nwidth : " + image.attr("width"));
			strImages.append("\nalt : " + image.attr("alt") + "\n\n");
		}

		return strImages.toString();
	}

	public String getURL() {
		return this.url;
	}

	public int getDistance() {
		return this.distance;
	}

	public String getURLSeed() {
		return urlSeed;
	}

	public double getPageRank() {
		return pageRank;
	}

	public void setPageRank(double pageRank) {
		this.pageRank = pageRank;
	}

	public String getSearchWord() {
		return keyword;
	}

	@Override
	public String toString() {
		return url;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof String) {
			return this.getURL().equals(obj);
		} else if (obj instanceof SpiderLeg) {
			return this.getURL().equals(((SpiderLeg) obj).getURL());
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		// Need to override this because of "!visited.add(nodeCurrent)"
		// operation
		return this.getURL().hashCode();
	}

	public static void main(String[] args) {
		String URL = "https://www.javatpoint.com/jsoup-example-print-images-of-an-url";

		SpiderLeg leg = new SpiderLeg(URL, 0, null, "");

		// System.out.println("\nImages: ");
		// System.out.println(leg.getImages());

		ArrayList<String> links = leg.getHyperLinks();
		System.out.println("\nLinks: ");
		for (String link : links) {
			System.out.println(link);
		}

		System.out.println("\nMetadata: ");
		System.out.println(leg.getMeta());
	}
}
