package main;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionListener;

import other.Constants;
import other.IconLaunch;
import other.Reusable;
import other.ThreadController.IThreadsActivityListener;
import views.JPanelMatchedURLs;
import views.JPanelSearchParameters;
import views.JPanelURLInfo;
import views.MenuBarView;
import views.StatusView;

/**
 * author: Dmytro Fomin ID 15898066 author: Nikolai Kolbenev ID 15897074
 * 
 * Well there are no main algorithms in this class. The functionality will be
 * described in accompanying pdf.
 */
public class ToolGUI extends JFrame {

	Collection<String> listURLs;
	JPanelSearchParameters jPanelSearchParameters;
	JPanelMatchedURLs jPanelMatchedURLs;
	JPanelURLInfo jPanelURLInfo;
	JButton jButtonLaunchSpiders;
	StatusView statusView;
	ListSelectionListener listSelectionListener;
	public static SpiderLeg selectedItem;
	MenuBarView menuBarView;

	public ToolGUI() {
		this.setTitle("Search Engine");
		this.setSize(1000, 550);
		this.setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		this.setLayout(new GridBagLayout());

		listURLs = new ArrayList<>();

		initLayout();
		loadLayout();
		attachHandlers();
	}

	private void initLayout() {
		menuBarView = new MenuBarView();
		statusView = new StatusView();
		jPanelSearchParameters = new JPanelSearchParameters(listURLs);
		jPanelMatchedURLs = new JPanelMatchedURLs();
		jPanelURLInfo = new JPanelURLInfo();
		jButtonLaunchSpiders = new JButton(Constants.LAUNCH_SPIDERS);
		jButtonLaunchSpiders.setIcon(new IconLaunch(jButtonLaunchSpiders));

		jPanelSearchParameters.listURLs.add(Constants.URL_JAVATPOINT);
		// jPanelSearchDetails.listURLs.add(Constants.URL_JSOUP_ORG);
		// jPanelSearchDetails.listURLs.add(Constants.URL_STACKOVERFLOW);
		jPanelSearchParameters.listURLs.add(Constants.URL_AUT_WEBSITE);
		jPanelSearchParameters.updateList();

		jPanelSearchParameters.jTextFieldSearchWord.setText(Constants.SEARCH_WORD);
	}

	private void loadLayout() {
		// gridx gridy gridwidth gridheight weightx weighty anchor fill insets
		// ipadx ipady
		// top left bottom right

		this.setJMenuBar(menuBarView);
		
		this.add(jPanelSearchParameters, new GridBagConstraints(0, 0, 1, 1, 0.0d, 1.0d, GridBagConstraints.PAGE_START,
				GridBagConstraints.BOTH, new Insets(15, 15, 0, 15), 0, 0));
		this.add(jPanelMatchedURLs, new GridBagConstraints(1, 0, 1, 1, 1.0d, 1.0d, GridBagConstraints.PAGE_START,
				GridBagConstraints.BOTH, new Insets(15, 15, 0, 15), 0, 0));
		this.add(jPanelURLInfo, new GridBagConstraints(2, 0, 1, 1, 0.0d, 1.0d, GridBagConstraints.PAGE_START,
				GridBagConstraints.BOTH, new Insets(15, 15, 0, 15), 0, 0));
		this.add(jButtonLaunchSpiders, new GridBagConstraints(0, 1, 2, 1, 0.0d, 0.0d, GridBagConstraints.LINE_END,
				GridBagConstraints.NONE, new Insets(2, 0, 10, 15), 0, 0));
		this.add(statusView, new GridBagConstraints(0, 2, 3, 1, 1.0d, 0.0d, GridBagConstraints.CENTER,
				GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
	}

	private void attachHandlers() {
		menuBarView.menuItemLoadVisited.addActionListener((event) -> {
			ProcessBuilder pb = new ProcessBuilder("notepad.exe", "data/visited.txt");
			try {
				pb.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		
		menuBarView.menuItemLoadMatched.addActionListener((event) -> {
			ProcessBuilder pb = new ProcessBuilder("notepad.exe", "data/matched.txt");
			try {
				pb.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		
		Spider.getMatchedNodes().addCollectionModifiedListener(() -> {
			this.repopulateListData();
			statusView.getInfoLabel().setText(getSearchStatus());
		});

		jButtonLaunchSpiders.addActionListener((event) -> {
			if (Spider.threadController.getThreadsRunning() > 0) {
				Spider.threadController.setKillThreads(true);
			} else {
				if (jPanelSearchParameters.listURLs.isEmpty()) {
					JOptionPane.showMessageDialog(this, "Specify Seed URLs first.");
				} else {
					selectedItem = null;
					Spider.searchKeywords = jPanelSearchParameters.jRadioButtonKeywords.isSelected()
							|| jPanelSearchParameters.jRadioButtonBoth.isSelected();
					Spider.searchDescription = jPanelSearchParameters.jRadioButtonDescription.isSelected()
							|| jPanelSearchParameters.jRadioButtonBoth.isSelected();

					jButtonLaunchSpiders.setText(Constants.STOP_SPIDERS);
					Spider.crawl(listURLs, (Integer) jPanelSearchParameters.jComboBoxDistance.getSelectedItem(),
							jPanelSearchParameters.jTextFieldSearchWord.getText().trim());
				}
			}
		});

		Spider.threadController.setURLProcessingListener(() -> {
			statusView.getInfoLabel().setText(getSearchStatus());
		});

		Spider.threadController.addThreadListener(new IThreadsActivityListener() {
			@Override
			public void onThreadDeath() {
				statusView.getInfoLabel().setText(getSearchStatus());
			}

			@Override
			public void onThreadBirth() {
				statusView.getInfoLabel().setText(getSearchStatus());
			}

			@Override
			public void beforeOnAllThreadsKilled() {
			}

			@Override
			public void onAllThreadsKilled() {
				jButtonLaunchSpiders.setText(Constants.LAUNCH_SPIDERS);
				statusView.getInfoLabel().setText(getSearchStatus());

				// Map<String, SpiderLeg> visited = Spider.getVisitedNodes();
				Collection<SpiderLeg> matchedArray = Spider.getMatchedNodes().getSpiderLegs();
				Map<String, SpiderLeg> matchedMap = new HashMap<>(matchedArray.size());
				for (SpiderLeg leg : matchedArray) {
					matchedMap.put(leg.getURL(), leg);
				}

				PageRank.updatePageRanks(matchedMap);
				repopulateListData();
			}
		});

		listSelectionListener = (event) -> {
			displayData();
		};
		jPanelMatchedURLs.jListSearchResults.addListSelectionListener(listSelectionListener);
		jPanelMatchedURLs.jComboBoxURLOrdering.addActionListener((event) -> handleComboBoxOrderingEvent());
	}

	public void handleComboBoxOrderingEvent() {
		String order = (String) jPanelMatchedURLs.jComboBoxURLOrdering.getSelectedItem();
		Comparator<SpiderLeg> comparator = Spider.getMatchedNodes().getLegOrderingComparator();
		Comparator<SpiderLeg> newComparator = comparator;
		switch (order) {
		case Constants.BY_URL:
			newComparator = (comparator == Reusable.getComparatorByURL()) ? comparator.reversed()
					: Reusable.getComparatorByURL();
			break;
		case Constants.BY_TITLE:
			newComparator = (comparator == Reusable.getComparatorByTitle()) ? comparator.reversed()
					: Reusable.getComparatorByTitle();
			break;
		case Constants.BY_DISTANCE:
			newComparator = (comparator == Reusable.getComparatorByDistance()) ? comparator.reversed()
					: Reusable.getComparatorByDistance();
			break;
		case Constants.BY_PAGE_RANK:
			newComparator = (comparator == Reusable.getComparatorByPageRank()) ? comparator.reversed()
					: Reusable.getComparatorByPageRank();
			break;
		}

		Spider.getMatchedNodes().setLegsOrderingComparator(newComparator);
	}

	public void repopulateListData() {
		jPanelMatchedURLs.jListSearchResults.setListData(Spider.getMatchedNodes().toList());
	}

	public void displayData() {
		SpiderLeg selected = (SpiderLeg) jPanelMatchedURLs.jListSearchResults.getSelectedValue();
		if (selected != null) {
			selectedItem = selected;
		}
		jPanelURLInfo.displayURLData(selectedItem);
	}

	public String getSearchStatus() {
		String status = "Status: " + Spider.threadController.getThreadsRunning()
				+ " spiders searching on the web. URLs visited: " + Spider.urls_visited_num + ". URLs matched: "
				+ Spider.getMatchedNodes().size() + ".";

		return status;
	}

	public static void main(String args[]) {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(ToolGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(ToolGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(ToolGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(ToolGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}

		ToolGUI hotPlate = new ToolGUI();
		hotPlate.setVisible(true);
	}

}
