package other;

/**
 * author: Dmytro Fomin ID 15898066
 * author: Nikolai Kolbenev ID 15897074
 * 
 * The interface to allow event subscriber to receive 
 * notification from the event source
 */
public interface Callback 
{
	public void call();
}
