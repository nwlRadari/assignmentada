package other;

/**
 * author: Dmytro Fomin ID 15898066
 * author: Nikolai Kolbenev ID 15897074
 *
 */
public class Constants {
	public static final int JSOUP_CONNECT_TIMEOUT = 3000;

	public static final int PADDING_FROM_WITHIN = 10;
	public static final String LAUNCH_SPIDERS = "Launch Spiders";
	public static final String STOP_SPIDERS = "Stop Spiders";

	public static final String URL_JAVATPOINT = "https://www.javatpoint.com";
	public static final String URL_JSOUP_ORG = "https://jsoup.org/";
	public static final String URL_STACKOVERFLOW = "https://stackoverflow.com/";
	public static final String URL_HELP_ECLIPSE_ORG = "https://help.eclipse.org";
	public static final String URL_AUT_WEBSITE = "http://www.aut.ac.nz/";

	public static final String SEARCH_WORD = "";

	public static final String BY_TITLE = "By Title";
	public static final String BY_PAGE_RANK = "By Page Rank";
	public static final String BY_DISTANCE = "By Distance";
	public static final String BY_URL = "By URL";

	public static final double START_PAGE_RANK = -1.0d;
}
