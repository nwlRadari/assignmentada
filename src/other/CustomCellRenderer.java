package other;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import main.ToolGUI;

/**
 * author: Dmytro Fomin ID 15898066
 * author: Nikolai Kolbenev ID 15897074
 *
 */
public class CustomCellRenderer extends JLabel implements ListCellRenderer  {
	Color highlightColor;

    public CustomCellRenderer() {
        setOpaque(true);
        highlightColor = new Color(20, 20, 150);
    }

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        String course = value.toString();
        this.setText(course);
        if (isSelected || value.equals(ToolGUI.selectedItem)) {
            setBackground(highlightColor);
            setForeground(Color.white);
        } else {
            setBackground(Color.white);
            setForeground(Color.black);
        }
        return this;
    }
}
