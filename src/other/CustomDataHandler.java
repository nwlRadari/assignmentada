package other;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

/**
 * author: Dmytro Fomin ID 15898066
 * author: Nikolai Kolbenev ID 15897074
 *
 */
public class CustomDataHandler<E> {
	private Comparator<E> legOrderingComparator;
	private Collection<E> spiderLegs;
	private ArrayList<Callback> eventListeners = new ArrayList<Callback>();
	
	public CustomDataHandler(Collection<E> collection)
	{
		if (collection == null)
		{
			throw new NullPointerException();
		}
		else
		{
			this.spiderLegs = collection;
		}
	}
	
	public void addCollectionModifiedListener(Callback listener)
	{
		eventListeners.add(listener);
	}
	
	private void notifyCollectionChanged()
	{
		for (Callback listener: eventListeners)
		{
			listener.call();
		}
	}
	
	public boolean contains(E leg)
	{
		return spiderLegs.contains(leg);
	}
	
	public void add(E leg)
	{
		if (leg != null)
		{
			spiderLegs.add(leg);
			notifyCollectionChanged();
		}
	}

//	public boolean remove(E leg)
//	{		
//		boolean removed = false;
//		if (leg != null)
//		{
//			removed = spiderLegs.remove(leg);
//		}
//		if (removed)
//		{
//			notifyCollectionChanged();	
//		}
//		
//		return removed;
//	}
	
	public void clear() {
		spiderLegs.clear();
		notifyCollectionChanged();
	}

	public Collection<E> getSpiderLegs() 
	{
		return spiderLegs;
	}

//	public void setSpiderLegs(Collection<E> legs) 
//	{
//		if (this.spiderLegs != legs)
//		{
//			this.spiderLegs = legs;
//			notifyCollectionChanged();
//		}
//	}

	public E[] toList()
	{
		E[] list = spiderLegs.toArray((E[])(new Object[spiderLegs.size()]));
		
//		Arrays.sort(list, getByTitleComparator());
		if (this.legOrderingComparator != null)
		{
			Arrays.sort(list, legOrderingComparator);
		}
		
		return list;
	}

	public void setLegsOrderingComparator(Comparator<E> legOrderingComparator) {
		this.legOrderingComparator = legOrderingComparator;
		notifyCollectionChanged();
	}


	public Comparator<E> getLegOrderingComparator() {
		return legOrderingComparator;
	}
	
	public int size() {
		return this.spiderLegs.size();
	}
}
