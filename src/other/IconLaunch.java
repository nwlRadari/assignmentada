package other;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.JButton;

/**
 * author: Dmytro Fomin ID 15898066
 * author: Nikolai Kolbenev ID 15897074
 *
 */
public class IconLaunch implements Icon {
	int iconWidth = 32;
	int iconHeight = 32;

	JButton owner;
	Color color;
	BasicStroke stroke;

	Image imageCrazyEmoji;
	Image imageNormalEmoji;

	public IconLaunch(JButton owner) {
		this.owner = owner;
		color = new Color(0, 155, 0);
		try {
			imageCrazyEmoji = ImageIO.read(this.getClass().getResource("/face1.png"));
			imageCrazyEmoji = imageCrazyEmoji.getScaledInstance(iconWidth, iconHeight, Image.SCALE_SMOOTH);

			imageNormalEmoji = ImageIO.read(this.getClass().getResource("/face2.jpg"));
			imageNormalEmoji = imageNormalEmoji.getScaledInstance(iconWidth, iconHeight, Image.SCALE_SMOOTH);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void paintIcon(Component c, Graphics g, int x, int y) {
		//Default location sucks...
		x = 5;
		y = (owner.getHeight() / 2 - iconHeight / 2);

		try {
			Graphics2D g2d = (Graphics2D) g.create();

			if (!owner.getModel().isPressed() && owner.getText().equals(Constants.LAUNCH_SPIDERS)) {
				g2d.drawImage(imageNormalEmoji, x, y, null);
			} else if (owner.getText().equals(Constants.STOP_SPIDERS)) {
				g2d.drawImage(imageCrazyEmoji, x, y, null);
			} else if (owner.getModel().isPressed() && owner.getText().equals(Constants.LAUNCH_SPIDERS)) {
				g2d.drawImage(imageCrazyEmoji, x, y, null);
			}

			g2d.dispose();
		} catch (Exception ex) {
			System.out.println(ex.toString());
			ex.printStackTrace();
		}
	}

	@Override
	public int getIconWidth() {
		return iconWidth;
	}

	@Override
	public int getIconHeight() {
		return iconHeight;
	}
}
