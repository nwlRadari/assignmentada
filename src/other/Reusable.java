package other;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.LinearGradientPaint;
import java.awt.MultipleGradientPaint;
import java.io.File;
import java.util.Comparator;

import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.StrokeBorder;
import javax.swing.border.TitledBorder;

import main.SpiderLeg;

/**
 * author: Dmytro Fomin ID 15898066
 * author: Nikolai Kolbenev ID 15897074
 *
 */
public class Reusable {

	public static void buildPath(String filePath)
	{
		buildPath(new File(filePath));
	}
	
	public static void buildPath(File file)
	{
		file.getParentFile().mkdirs();
	}
	
	public static Comparator<SpiderLeg> getComparatorByTitle() {
		return (leg1, leg2) -> leg1.getTitle().compareTo(leg2.getTitle());
	}

	public static Comparator<SpiderLeg> getComparatorByPageRank() {
		//Decreasing order by default as required
		return (leg1, leg2) -> Double.compare(leg2.getPageRank(), leg1.getPageRank()); 
	}
	
	public static Comparator<SpiderLeg> getComparatorByDistance() {
		return (leg1, leg2) -> Integer.compare(leg1.getDistance(), leg2.getDistance());
	}
	
	public static Comparator<SpiderLeg> getComparatorByURL() {
		return (leg1, leg2) -> leg1.getURL().compareTo(leg2.getURL());
	}

	public static Border getDefaultBorder(int width, int height) {
		return BorderFactory.createLineBorder(Color.blue, 2);
	}

	public static TitledBorder getTwoColorGradientCyan(int width, int height) {
		return getTwoColorGradient(Color.CYAN, width, height);
	}

	public static TitledBorder getTwoColorGradientRed(int width, int height) {
		return getTwoColorGradient(Color.RED, width, height);
	}

	private static TitledBorder getTwoColorGradient(Color color, int width, int height) {
		GradientPaint twoColorPaint = new GradientPaint(0.0f, 0.0f, Color.BLUE, 50.0f, 50.0f, color, true);
		return new TitledBorder(new StrokeBorder(new BasicStroke(3), twoColorPaint), "", TitledBorder.CENTER,
				TitledBorder.TOP);
	}

	public static TitledBorder getRainbowGradient(int width, int height) {
		Color[] colors = { Color.CYAN, Color.BLUE, Color.PINK, Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN };
		float[] fractions = new float[7];
		for (int i = 0; i < fractions.length; i++) {
			fractions[i] = i / (float) fractions.length;
		}

		MultipleGradientPaint rainbowPaint = new LinearGradientPaint(0.0f, 0.0f, width, height, fractions, colors);
		return new TitledBorder(new StrokeBorder(new BasicStroke(3), rainbowPaint), "", TitledBorder.CENTER,
				TitledBorder.TOP);
	}
}
