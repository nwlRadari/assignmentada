package other;

import java.util.ArrayList;

/**
 * author: Dmytro Fomin ID 15898066 author: Nikolai Kolbenev ID 15897074
 *
 * This class controls multithreading when doing a search with many spiders.
 * Each spider is treated as a separate thread. When a thread is created, it
 * increases the number of spiders running (threads running). This allows to
 * track progress and update UI accordingly. This is also useful for initiating
 * the Page Rank algorithm after all spiders are done with their search (i.e.
 * when the listeners are invoked by onAllThreadsKilled).
 */
public class ThreadController {
	public interface IThreadsActivityListener {
		void beforeOnAllThreadsKilled();

		void onAllThreadsKilled();

		void onThreadDeath(); // Needed to update number of Spiders running

		void onThreadBirth(); // Needed to update number of Spiders running
	}

	public interface IURLProcessingListener {
		void visitedURLsChanged();
	}

	private ArrayList<IThreadsActivityListener> listThreadsListeners;
	private IURLProcessingListener urlProcessingListener;
	private int threadsRunning = 0;
	private boolean isKillThreads;

	public ThreadController() {
		listThreadsListeners = new ArrayList<>();
	}

	public synchronized void increaseThreadsRunning() {
		threadsRunning++;
		for (IThreadsActivityListener listener : listThreadsListeners) {
			listener.onThreadBirth();
		}
	}

	public synchronized void decreaseThreadsRunning() {
		threadsRunning--;
		if (threadsRunning == 0) {
			for (IThreadsActivityListener listener : listThreadsListeners) {
				listener.beforeOnAllThreadsKilled();
				listener.onAllThreadsKilled();
				isKillThreads = false;
			}
		} else if (threadsRunning > 0) {
			for (IThreadsActivityListener listener : listThreadsListeners) {
				listener.onThreadDeath();
			}
		} else {
			System.err.println("Unexpected threads running: " + threadsRunning);
		}
	}

	public void addThreadListener(IThreadsActivityListener threadsListener) {
		if (threadsListener != null) {
			listThreadsListeners.add(threadsListener);
			if (threadsRunning == 0) {
				threadsListener.beforeOnAllThreadsKilled();
				threadsListener.onAllThreadsKilled();
			}
		}
	}

	public void setURLProcessingListener(IURLProcessingListener listener) {
		this.urlProcessingListener = listener;
	}

	public void notifyVisitedURLsChanged() {
		if (this.urlProcessingListener != null) {
			urlProcessingListener.visitedURLsChanged();
		}
	}

	public boolean isKillThreads() {
		return isKillThreads;
	}

	public void setKillThreads(boolean isKillThreads) {
		this.isKillThreads = isKillThreads;
	}

	public int getThreadsRunning() {
		return threadsRunning;
	}
}
