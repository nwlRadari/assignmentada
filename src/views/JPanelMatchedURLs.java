package views;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import other.Constants;
import other.CustomCellRenderer;

/**
 * author: Dmytro Fomin ID 15898066
 * author: Nikolai Kolbenev ID 15897074
 *
 */
public class JPanelMatchedURLs extends JPanel {
	
    JPanel jPanelTop;
    JPanel jPanelBottom;
	
	JLabel jLabelSearchResults;
	public JComboBox<String> jComboBoxURLOrdering;
	
	JScrollPane jScrollPaneSearchResults;
    public JList<Object> jListSearchResults;
    
	public JPanelMatchedURLs() {
		this.setLayout(new GridBagLayout());
		
		initLayout();
		loadLayout();
	}
	
	private void initLayout() {
		jPanelTop = new JPanel(new GridBagLayout());
		jPanelBottom = new JPanel(new GridBagLayout());
		
		jLabelSearchResults = new JLabel("Matched URLs");
		jComboBoxURLOrdering = new JComboBox<String>(new String[]{Constants.BY_URL, Constants.BY_TITLE, Constants.BY_DISTANCE, Constants.BY_PAGE_RANK});
		jComboBoxURLOrdering.setSelectedItem(Constants.BY_PAGE_RANK);
		
		jListSearchResults = new JList<>();
		jListSearchResults.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jListSearchResults.setCellRenderer(new CustomCellRenderer());
		jScrollPaneSearchResults = new JScrollPane(jListSearchResults);
	}
	
	private void loadLayout() {
		//gridx gridy gridwidth gridheight weightx weighty anchor fill insets ipadx ipady
        //top left bottom right
		
		
		this.add(jLabelSearchResults, new GridBagConstraints(0, 0, 1, 1, 1.0d, 0.0d,
                GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		this.add(jComboBoxURLOrdering, new GridBagConstraints(1, 0, 1, 1, 0.0d, 0.0d,
                GridBagConstraints.LINE_END, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		this.add(jScrollPaneSearchResults, new GridBagConstraints(0, 1, 2, 1, 1.0d, 1.0d,
                GridBagConstraints.FIRST_LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
}
