package views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Collection;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;

import other.Constants;
import other.CustomCellRenderer;
import other.Reusable;

/**
 * author: Dmytro Fomin ID 15898066
 * author: Nikolai Kolbenev ID 15897074
 *
 */
public class JPanelSearchParameters extends JPanel {
	
	public Collection<String> listURLs;
	
	JPanel jPanelDetails;
    JPanel jPanelSearchParameters;
    JPanel jPanelSeedURLs;
	
	JLabel jLabelSearchDetails;
	
	JLabel jLabelSearchWord;
	public JTextField jTextFieldSearchWord;
	
	JLabel jLabelSearchIn;
	public JRadioButton jRadioButtonKeywords;
	public JRadioButton jRadioButtonDescription;
	public JRadioButton jRadioButtonBoth;
	
	JLabel jLabelDistance;
	public JComboBox<Integer> jComboBoxDistance;
	
	JLabel jLabelNewSeedURL;
	JTextField jTextFieldNewSeedURL;
	
	JButton jButtonPasteURL;
	JButton jButtonAddURL;
	
	JLabel jLabelSeedURLs;
	JScrollPane jScrollPaneSeedURLs;
    JList<Object> jListSeedURLs;
    JButton jButtonRemoveSeedURL;
	
	public JPanelSearchParameters(Collection<String> urls) {
		this.setLayout(new GridBagLayout());
		this.listURLs = urls;
		
		this.setPreferredSize(new Dimension(320, this.getPreferredSize().height));
		this.setMinimumSize(new Dimension(320, this.getPreferredSize().height));
		
		initLayout();
		loadLayout();
		attachHandlers();
		updateList();
		updateButtonStates();
	}
	
	private void initLayout() {
		jPanelDetails = new JPanel(new GridBagLayout());
		jPanelDetails.setBorder(Reusable.getTwoColorGradientCyan(jPanelDetails.getWidth(), jPanelDetails.getHeight()));
		
		jPanelSearchParameters = new JPanel(new GridBagLayout());
		jPanelSearchParameters.setBorder(new CompoundBorder(new MatteBorder(0, 0, 2, 0, Color.blue), new EmptyBorder(0, 0, 10, 0)));
		jPanelSeedURLs = new JPanel(new GridBagLayout());

		jLabelSearchDetails = new JLabel("Search Parameters");
		jLabelDistance = new JLabel("Distance:");
		jComboBoxDistance = new JComboBox<>(new Integer[]{0,1,2,3,4,5,6,7,8,9});
		jComboBoxDistance.setSelectedIndex(1);
		jComboBoxDistance.setPreferredSize(new Dimension(60, jComboBoxDistance.getPreferredSize().height));
		
		jLabelSearchIn = new JLabel("Search In: ");
		jRadioButtonKeywords = new JRadioButton("Keywords");
		jRadioButtonKeywords.setSelected(true);
		jRadioButtonDescription = new JRadioButton("Description");
		jRadioButtonBoth = new JRadioButton("Both");
		
		jLabelSearchWord = new JLabel("Search Word: ");
		jTextFieldSearchWord = new JTextField();
		
		ButtonGroup group = new ButtonGroup();
		group.add(jRadioButtonKeywords);
		group.add(jRadioButtonDescription);
		group.add(jRadioButtonBoth);
		
		jLabelNewSeedURL = new JLabel("New Seed URL: ");
		jTextFieldNewSeedURL = new JTextField();
		
		jButtonPasteURL = new JButton("Paste URL");
		jButtonAddURL = new JButton("Add URL");
		
		jLabelSeedURLs = new JLabel("List of Seed URLs");
		jListSeedURLs = new JList<>();
		jListSeedURLs.setCellRenderer(new CustomCellRenderer());
		jScrollPaneSeedURLs = new JScrollPane(jListSeedURLs);
		jButtonRemoveSeedURL = new JButton("Remove URL");
	}
	
	private void loadLayout() {
		//gridx gridy gridwidth gridheight weightx weighty anchor fill insets ipadx ipady
        //top left bottom right
		
		jPanelSearchParameters.add(jLabelSearchWord, new GridBagConstraints(0, 0, 1, 1, 0.0d, 0.0d,
                GridBagConstraints.LINE_START, GridBagConstraints.NONE, new Insets(Constants.PADDING_FROM_WITHIN, Constants.PADDING_FROM_WITHIN, 0, 0), 0, 0));
		jPanelSearchParameters.add(jTextFieldSearchWord, new GridBagConstraints(1, 0, 3, 1, 1.0d, 0.0d,
                GridBagConstraints.LINE_START, GridBagConstraints.HORIZONTAL, new Insets(Constants.PADDING_FROM_WITHIN, 2, 0, Constants.PADDING_FROM_WITHIN), 0, 0));
		
		jPanelSearchParameters.add(jLabelSearchIn, new GridBagConstraints(0, 1, 1, 1, 0.0d, 0.0d,
                GridBagConstraints.LINE_START, GridBagConstraints.NONE, new Insets(Constants.PADDING_FROM_WITHIN, Constants.PADDING_FROM_WITHIN, 0, 0), 0, 0));
		jPanelSearchParameters.add(jRadioButtonKeywords, new GridBagConstraints(1, 1, 1, 1, 1.0d, 0.0d,
                GridBagConstraints.LINE_START, GridBagConstraints.HORIZONTAL, new Insets(Constants.PADDING_FROM_WITHIN, 0, 0, 2), 0, 0));
		jPanelSearchParameters.add(jRadioButtonDescription, new GridBagConstraints(2, 1, 1, 1, 1.0d, 0.0d,
                GridBagConstraints.LINE_START, GridBagConstraints.HORIZONTAL, new Insets(Constants.PADDING_FROM_WITHIN, 0, 0, 2), 0, 0));
		jPanelSearchParameters.add(jRadioButtonBoth, new GridBagConstraints(3, 1, 1, 1, 1.0d, 0.0d,
                GridBagConstraints.LINE_START, GridBagConstraints.HORIZONTAL, new Insets(Constants.PADDING_FROM_WITHIN, 0, 0, Constants.PADDING_FROM_WITHIN), 0, 0));
		
		jPanelSearchParameters.add(jLabelDistance, new GridBagConstraints(0, 2, 1, 1, 0.0d, 0.0d,
                GridBagConstraints.LINE_START, GridBagConstraints.NONE, new Insets(Constants.PADDING_FROM_WITHIN, Constants.PADDING_FROM_WITHIN, 0, Constants.PADDING_FROM_WITHIN), 0, 0));
		jPanelSearchParameters.add(jComboBoxDistance, new GridBagConstraints(1, 2, 1, 1, 0.0d, 0.0d,
                GridBagConstraints.LINE_START, GridBagConstraints.HORIZONTAL, new Insets(Constants.PADDING_FROM_WITHIN, 2, 0, Constants.PADDING_FROM_WITHIN), 0, 0));
		
        jPanelSeedURLs.add(jLabelNewSeedURL, new GridBagConstraints(0, 0, 1, 1, 0.0d, 0.0d,
                GridBagConstraints.LINE_START, GridBagConstraints.NONE, new Insets(10, Constants.PADDING_FROM_WITHIN, 0, 0), 0, 0));
        jPanelSeedURLs.add(jTextFieldNewSeedURL, new GridBagConstraints(1, 0, 1, 1, 1.0d, 0.0d,
                GridBagConstraints.LINE_START, GridBagConstraints.HORIZONTAL, new Insets(10, 2, 0, Constants.PADDING_FROM_WITHIN), 0, 0));

        JPanel jPanelTopButtons = new JPanel();
        jPanelTopButtons.add(jButtonPasteURL);
        jPanelTopButtons.add(jButtonAddURL);
        jPanelSeedURLs.add(jPanelTopButtons, new GridBagConstraints(0, 1, 2, 1, 0.0d, 0.0d,
                GridBagConstraints.LINE_END, GridBagConstraints.NONE, new Insets(0, 0, 0, Constants.PADDING_FROM_WITHIN), 0, 0));

        jPanelSeedURLs.add(jLabelSeedURLs, new GridBagConstraints(0, 2, 2, 1, 1.0d, 0.0d,
                GridBagConstraints.LINE_START, GridBagConstraints.BOTH, new Insets(0, 2+Constants.PADDING_FROM_WITHIN, 2, 0), 0, 0));
        jPanelSeedURLs.add(jScrollPaneSeedURLs, new GridBagConstraints(0, 3, 2, 1, 1.0d, 1.0d,
                GridBagConstraints.LINE_START, GridBagConstraints.BOTH, new Insets(0, Constants.PADDING_FROM_WITHIN, 0, Constants.PADDING_FROM_WITHIN), 0, 0));
        jPanelSeedURLs.add(jButtonRemoveSeedURL, new GridBagConstraints(1, 4, 1, 1, 0.0d, 0.0d,
                GridBagConstraints.LINE_END, GridBagConstraints.NONE, new Insets(0, 0, Constants.PADDING_FROM_WITHIN, Constants.PADDING_FROM_WITHIN), 0, 0));

        
        jPanelDetails.add(jPanelSearchParameters, new GridBagConstraints(0, 1, 1, 1, 1.0d, 0.0d,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        jPanelDetails.add(jPanelSeedURLs, new GridBagConstraints(0, 2, 1, 1, 1.0d, 1.0d,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        
        this.add(jLabelSearchDetails, new GridBagConstraints(0, 0, 1, 1, 1.0d, 0.0d,
                GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        this.add(jPanelDetails, new GridBagConstraints(0, 1, 1, 1, 1.0d, 1.0d,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}
	
	private void attachHandlers() {
        jButtonPasteURL.addActionListener((event) -> {
            if (jListSeedURLs.getSelectedValue() == null) {
                JOptionPane.showMessageDialog(this, "Select the URL to paste into textfield above.");
            } else {
                jTextFieldNewSeedURL.setText(jListSeedURLs.getSelectedValue().toString());
            }
        });

        jButtonAddURL.addActionListener((event) -> {
            String newCourse = jTextFieldNewSeedURL.getText().trim();

            if (newCourse.isEmpty()) {
                JOptionPane.showMessageDialog(this, "You should enter the URL.");
            } else if (listURLs.contains(newCourse)) {
                JOptionPane.showMessageDialog(this, "The URL is already in the list.");
            } else {
            	listURLs.add(newCourse);
                updateList();
            }
        });

        jButtonRemoveSeedURL.addActionListener((event) -> {
            List<Object> selectedCourses = jListSeedURLs.getSelectedValuesList();
            if (selectedCourses.isEmpty()) {
                JOptionPane.showMessageDialog(this, "Select courses first.");
            } else {

                int returnVal = JOptionPane.showOptionDialog(this, "Do you want to remove highlighted URLs from the list?",
                        "Delete Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);

                if (returnVal == JOptionPane.YES_OPTION) {
                    for (Object course : selectedCourses) {
                    	listURLs.remove(course);
                    }
                    updateList();
                }
            }
        });

        jListSeedURLs.addListSelectionListener((event) -> {
            updateButtonStates();
        });
    }

    public void updateList() {
        jListSeedURLs.setListData(listURLs.toArray());
    }
    
    private void updateButtonStates() {
    	jButtonPasteURL.setEnabled(jListSeedURLs.getSelectedValue() != null);
        jButtonRemoveSeedURL.setEnabled(jListSeedURLs.getSelectedValue() != null);
    }
}
