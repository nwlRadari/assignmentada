package views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.text.Highlighter.HighlightPainter;

import main.SpiderLeg;
import other.Constants;
import other.Reusable;

/**
 * author: Dmytro Fomin ID 15898066
 * author: Nikolai Kolbenev ID 15897074
 *
 */
public class JPanelURLInfo extends JPanel {
	JPanel jPanelInfo;
	JLabel jLabelURLInfo;

	JLabel jLabelURLtitle;
	JLabel jLabelURLMetaDescription;
	JLabel jLabelURLMetaKeywords;
	JLabel jLabelURLPageRank;
	JLabel jLabelURLDistance;
	JLabel jLabelSeedURL;

	JTextField jTextFieldURLtitle;
	JTextArea jTextAreaURLMetaDescription;
	JTextArea jTextAreaURLMetaKeywords;
	JTextField jTextFieldSeedURL;

	JScrollPane jScrollPaneMetaDescription;
	JScrollPane jScrollPaneMetaKeywords;

	public JPanelURLInfo() {
		this.setLayout(new GridBagLayout());

		initLayout();
		loadLayout();
	}

	private void initLayout() {
		jPanelInfo = new JPanel(new GridBagLayout());
		jLabelURLInfo = new JLabel("URL Info");

		jLabelURLtitle = new JLabel("Title ");
		jLabelURLMetaDescription = new JLabel("Meta Description ");
		jLabelURLMetaKeywords = new JLabel("Meta Keywords ");
		jLabelURLPageRank = new JLabel("Page Rank: ?");
		jLabelURLDistance = new JLabel("Distance From Seed URL: ?");
		jLabelSeedURL = new JLabel("Seed URL ");

		jTextFieldURLtitle = new JTextField();
		jTextFieldURLtitle.setEditable(false);
		jTextFieldSeedURL = new JTextField();
		jTextFieldSeedURL.setEditable(false);

		int sizeScale = 4;

		jTextAreaURLMetaKeywords = new JTextArea();
		jTextAreaURLMetaKeywords.setLineWrap(true);
		jTextAreaURLMetaKeywords.setWrapStyleWord(true);
		jTextAreaURLMetaKeywords.setEditable(false);

		jTextAreaURLMetaDescription = new JTextArea();
		jTextAreaURLMetaDescription.setLineWrap(true);
		jTextAreaURLMetaDescription.setWrapStyleWord(true);
		jTextAreaURLMetaDescription.setEditable(false);

		jScrollPaneMetaDescription = new JScrollPane(jTextAreaURLMetaDescription,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		jScrollPaneMetaDescription.setPreferredSize(new Dimension(jScrollPaneMetaDescription.getPreferredSize().width,
				sizeScale * jScrollPaneMetaDescription.getPreferredSize().height));
		jScrollPaneMetaKeywords = new JScrollPane(jTextAreaURLMetaKeywords, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		jScrollPaneMetaKeywords.setPreferredSize(new Dimension(jScrollPaneMetaKeywords.getPreferredSize().width,
				sizeScale * jScrollPaneMetaKeywords.getPreferredSize().height));

		this.setPreferredSize(new Dimension(300, this.getPreferredSize().height));
		this.setMinimumSize(new Dimension(300, this.getPreferredSize().height));
	}

	private void loadLayout() {
		// gridx gridy gridwidth gridheight weightx weighty anchor fill insets
		// ipadx ipady
		// top left bottom right

		int mainAnchor = GridBagConstraints.LINE_START;
		int mainFillLabels = GridBagConstraints.NONE;
		int mainFillText = GridBagConstraints.HORIZONTAL;

		jPanelInfo.add(jLabelURLtitle, new GridBagConstraints(0, 0, 1, 1, 0.0d, 0.0d, mainAnchor, mainFillLabels,
				new Insets(Constants.PADDING_FROM_WITHIN, Constants.PADDING_FROM_WITHIN, 0, 0), 0, 0));
		jPanelInfo.add(jLabelSeedURL, new GridBagConstraints(0, 1, 1, 1, 0.0d, 0.0d, mainAnchor, mainFillLabels,
				new Insets(0, Constants.PADDING_FROM_WITHIN, 0, 0), 0, 0));
		jPanelInfo.add(jLabelURLMetaKeywords,
				new GridBagConstraints(0, 2, 1, 1, 0.0d, 0.0d, GridBagConstraints.FIRST_LINE_START, mainFillLabels,
						new Insets(3, Constants.PADDING_FROM_WITHIN, 0, 0), 0, 0));
		jPanelInfo.add(jLabelURLMetaDescription,
				new GridBagConstraints(0, 3, 1, 1, 0.0d, 0.0d, GridBagConstraints.FIRST_LINE_START, mainFillLabels,
						new Insets(3, Constants.PADDING_FROM_WITHIN, 0, 0), 0, 0));
		jPanelInfo.add(jLabelURLPageRank, new GridBagConstraints(0, 4, 2, 1, 0.0d, 0.0d, mainAnchor, mainFillLabels,
				new Insets(Constants.PADDING_FROM_WITHIN, Constants.PADDING_FROM_WITHIN, 0, 0), 0, 0));
		jPanelInfo.add(jLabelURLDistance,
				new GridBagConstraints(0, 5, 2, 1, 0.0d, 0.0d, mainAnchor, mainFillLabels,
						new Insets(Constants.PADDING_FROM_WITHIN, Constants.PADDING_FROM_WITHIN,
								Constants.PADDING_FROM_WITHIN, 0),
						0, 0));

		jPanelInfo.add(jTextFieldURLtitle,
				new GridBagConstraints(1, 0, 1, 1, 1.0d, 0.0d, mainAnchor, GridBagConstraints.HORIZONTAL,
						new Insets(Constants.PADDING_FROM_WITHIN, 0, 0, Constants.PADDING_FROM_WITHIN), 0, 0));
		jPanelInfo.add(jTextFieldSeedURL, new GridBagConstraints(1, 1, 1, 1, 1.0d, 0.0d, mainAnchor,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, Constants.PADDING_FROM_WITHIN), 0, 0));
		jPanelInfo.add(jScrollPaneMetaKeywords, new GridBagConstraints(1, 2, 1, 1, 1.0d, 1.0d, mainAnchor,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, Constants.PADDING_FROM_WITHIN), 0, 0));
		jPanelInfo.add(jScrollPaneMetaDescription, new GridBagConstraints(1, 3, 1, 1, 1.0d, 1.0d, mainAnchor,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, Constants.PADDING_FROM_WITHIN), 0, 0));

		jPanelInfo.setBorder(Reusable.getTwoColorGradientCyan(jPanelInfo.getWidth(), jPanelInfo.getHeight()));

		this.add(jLabelURLInfo, new GridBagConstraints(0, 0, 1, 1, 1.0d, 0.0d, GridBagConstraints.CENTER,
				GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
		this.add(jPanelInfo, new GridBagConstraints(0, 1, 1, 1, 1.0d, 1.0d, GridBagConstraints.CENTER,
				GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
	}

	public void displayURLData(SpiderLeg leg) {
		String title = "";
		String seedURL = "";
		String distance = "Distance From Seed URL: ";
		String pageRank = "Page Rank: ";
		String metaDesc = "";
		String metaKeyword = "";

		if (leg != null) {
			title = leg.getTitle();
			seedURL = leg.getURLSeed();
			distance = distance + String.valueOf(leg.getDistance());
			pageRank += (leg.getPageRank() == Constants.START_PAGE_RANK) ? "?" : String.valueOf(leg.getPageRank());

			metaDesc = leg.getMetaDescription();
			metaKeyword = leg.getMetaKeywords();
			jTextAreaURLMetaDescription.setText(metaDesc);
			jTextAreaURLMetaKeywords.setText(metaKeyword);
			addHighlight(jTextAreaURLMetaDescription, leg.getSearchWord());
			addHighlight(jTextAreaURLMetaKeywords, leg.getSearchWord());
		} else {
			distance = distance + "?";
			pageRank = pageRank + "?";
			jTextAreaURLMetaDescription.setText(metaDesc);
			jTextAreaURLMetaKeywords.setText(metaKeyword);
		}

		jTextFieldURLtitle.setText(title);
		jTextFieldSeedURL.setText(seedURL);
		jLabelURLDistance.setText(distance);
		jLabelURLPageRank.setText(pageRank);

		jTextFieldURLtitle.setCaretPosition(0);
		jTextFieldSeedURL.setCaretPosition(0);
		jTextAreaURLMetaDescription.setCaretPosition(0);
		jTextAreaURLMetaKeywords.setCaretPosition(0);
	}

	private void addHighlight(JTextArea area, String searchWord) {
		if (area.getText().trim().isEmpty() || searchWord == null || searchWord.trim().isEmpty()) {
			return;
		}
		Highlighter highlighter = area.getHighlighter();
		highlighter.removeAllHighlights();

		HighlightPainter p = new DefaultHighlighter.DefaultHighlightPainter(Color.YELLOW);

		searchWord = searchWord.toLowerCase();
		String text = area.getText().toLowerCase();
		int index = text.indexOf(searchWord, 0);
		while (index != -1) {
			try {
				highlighter.addHighlight(index, index + searchWord.length(), p);
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
			index = text.indexOf(searchWord, index + searchWord.length());
		}
	}
}
