package views;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * author: Dmytro Fomin ID 15898066
 * author: Nikolai Kolbenev ID 15897074
 *
 */
public class MenuBarView extends JMenuBar {
	private JMenu menu;
	public JMenuItem menuItemLoadVisited;
	public JMenuItem menuItemLoadMatched;

	public MenuBarView() {
		initComponents();
		loadComponents();
	}

	private void initComponents() {
		menu = new JMenu("File");
		menuItemLoadVisited = new JMenuItem("Show visited");
		menuItemLoadMatched = new JMenuItem("Show matched");
	}

	private void loadComponents() {
		menu.add(menuItemLoadVisited);
		menu.add(menuItemLoadMatched);

		this.add(menu);
	}
}
