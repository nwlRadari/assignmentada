package views;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

/**
 * author: Dmytro Fomin ID 15898066
 * author: Nikolai Kolbenev ID 15897074
 *
 */
public class StatusView extends JPanel {
	private JLabel labelInfo;

    public StatusView()  {
        this.setBorder(new BevelBorder(BevelBorder.LOWERED));

        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

        labelInfo = new JLabel("Status: ");
        labelInfo.setHorizontalAlignment(SwingConstants.LEFT);

        this.add(labelInfo);
    }

    public JLabel getInfoLabel() {
        return labelInfo;
    }
}
